/*
 *  Copyright (c) 2019 Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 *  Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 *  Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 */

package io.entgra.iot.fog.usermgt.dao;

import io.entgra.iot.fog.usermgt.beans.User;
import io.entgra.iot.fog.usermgt.exceptions.UserManagementException;
import io.entgra.iot.fog.usermgt.utils.CommonUtil;
import io.entgra.iot.fog.usermgt.utils.DataBaseUtil;
import io.entgra.iot.fog.usermgt.utils.Secret;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class UserManagementGenericDAOImpl implements UserManagementDAO {
    private static final Logger log = LogManager.getLogger(UserManagementGenericDAOImpl.class);
    private static UserManagementDAO userManagementDAO;
    private static final String COLUMN_LABEL_USERNAME = "username";
    private static final String COLUMN_LABEL_ATTRIBUTES = "attributes";
    private static final String COLUMN_LABEL_ROLES = "roles";

    private UserManagementGenericDAOImpl() {}

    /**
     * Get {@link UserManagementDAO} instance
     * @return {@link UserManagementDAO} implementation
     */
    public static UserManagementDAO getUserManagementDAO() {
        if (userManagementDAO == null) {
            userManagementDAO = new UserManagementGenericDAOImpl();
        }
        return userManagementDAO;
    }

    /**
     * Persists the user
     *
     * @param user {@link User} object
     * @throws UserManagementException if error occurred when persisting the user
     */
    @Override
    public void addUser(User user) throws UserManagementException {
        Connection dbConnection = null;
        Secret credentialObj = null;

        try {
            dbConnection = DataBaseUtil.getDBConnection();
            credentialObj = Secret.getSecret(user.getPassword());
            String sqlStmt = "INSERT INTO UM_USER (UM_USER_NAME, UM_USER_PASSWORD, UM_SALT_VALUE, UM_REQUIRE_CHANGE, "
                    + "UM_CHANGED_TIME) VALUES (?, ?, ?, ?, ?)";

            String saltValue = CommonUtil.generateSaltValue();
            String password = CommonUtil.preparePassword(credentialObj, saltValue);

            // do all 4 possibilities
            if (saltValue == null) {
                CommonUtil.updateStringValuesToDatabase(dbConnection, sqlStmt, user.getUserName(), password, "", false,
                        new Date());
            } else {
                CommonUtil.updateStringValuesToDatabase(dbConnection, sqlStmt, user.getUserName(), password, saltValue,
                        false, new Date());
            }

            if (user.getRoles() != null && user.getRoles().length > 0) {
                String[] roles = user.getRoles();
                String sqlStmt2;
                if (roles.length > 0) {
                    // Adding user to the non shared roles
                    sqlStmt2 = "INSERT INTO UM_USER_ROLE (UM_ROLE_ID, "
                            + "UM_USER_ID) VALUES ((SELECT UM_ID FROM UM_ROLE WHERE UM_ROLE_NAME=?),"
                            + "(SELECT UM_ID FROM UM_USER WHERE LOWER(UM_USER_NAME)=LOWER(?)))";
                    DataBaseUtil.updateUserRoleMappingInBatchMode(dbConnection, sqlStmt2, roles, user.getUserName());
                }
            }

            if (user.getClaims() != null) {
                DataBaseUtil.addUserAttributes(dbConnection, user.getUserName(), user.getClaims());
            }

            dbConnection.commit();
        } catch (Exception e) {
            try {
                if (dbConnection != null) {
                    dbConnection.rollback();
                }
            } catch (SQLException e1) {
                String errorMessage = "Error roll backing add user operation for user : " + user.getUserName();
                if (log.isDebugEnabled()) {
                    log.debug(errorMessage, e1);
                }
            }
            throw new UserManagementException("Error while persisting user : " + user.getUserName(), e);
        } finally {
            if (credentialObj != null) {
                credentialObj.clear();
            }
            DataBaseUtil.closeAllConnections(dbConnection);
        }
    }

    /**
     * Retrieves user for a given username
     *
     * @param username username
     * @return {@link User} object of the given username
     * @throws UserManagementException if unable to fetch the user.
     */
    @Override
    public User getUser(String username) throws UserManagementException {
        Connection dbConnection = null;
        PreparedStatement prepStmt = null;
        User user = new User();

        String getUserStatement = "SELECT"
                + " uid, "
                + " username, "
                + " GROUP_CONCAT(DISTINCT CONCAT_WS(':', att.UM_ATTR_NAME, att.UM_ATTR_VALUE)) AS attributes, "
                + " GROUP_CONCAT(DISTINCT r.UM_ROLE_NAME) AS roles "
                + "FROM "
                + " UM_USER_ROLE ur, "
                + " UM_ROLE r, "
                + " (SELECT u.UM_ID AS uid, u.UM_USER_NAME AS username FROM UM_USER u WHERE u.UM_USER_NAME = ?) AS USER"
                + " LEFT JOIN UM_USER_ATTRIBUTE att ON uid = att.UM_USER_ID "
                + "WHERE"
                + " uid = ur.UM_USER_ID AND ur.UM_ROLE_ID = r.UM_ID;";

        try {
            dbConnection = DataBaseUtil.getDBConnection();
            prepStmt = dbConnection.prepareStatement(getUserStatement);
            prepStmt.setString(1, username);

            ResultSet rs = prepStmt.executeQuery();

            if (rs.next()) {
                user.setUserName(rs.getString(COLUMN_LABEL_USERNAME));
                String attributes = rs.getString(COLUMN_LABEL_ATTRIBUTES);

                if (StringUtils.isNotEmpty(attributes)) {
                    Map<String, String> claims = Pattern.compile(",").splitAsStream(attributes)
                            .map(attribute -> attribute.split(":")).collect(Collectors
                                    .toMap(attribute -> attribute[0], attribute -> attribute[1], (p1, p2) -> p1));
                    user.setClaims(claims);
                }

                String roles = rs.getString(COLUMN_LABEL_ROLES);
                if (roles != null) {
                    String[] roleList = Pattern.compile(",").splitAsStream(roles).toArray(String[]::new);
                    user.setRoles(roleList);
                }
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new UserManagementException("Error occurred while retrieving the user list.", e);
        } finally {
            DataBaseUtil.closeAllConnections(dbConnection, prepStmt);
        }
        return user;
    }

    @Override
    public String getUserName(String claim) throws UserManagementException {
        Connection dbConnection = null;
        PreparedStatement prepStmt = null;
        User user = new User();

        String getUserStatement = "SELECT * FROM UM_USER WHERE UM_ID = (SELECT UM_USER_ID FROM UM_USER_ATTRIBUTE " +
                "WHERE UM_ATTR_VALUE=?)";

        try {
            dbConnection = DataBaseUtil.getDBConnection();
            prepStmt = dbConnection.prepareStatement(getUserStatement);
            prepStmt.setString(1, claim);

            ResultSet rs = prepStmt.executeQuery();

            if (rs.next()) {
                return rs.getString("UM_USER_NAME");
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new UserManagementException("Error occurred while retrieving the user list.", e);
        } finally {
            DataBaseUtil.closeAllConnections(dbConnection, prepStmt);
        }
    }

    @Override
    public List<User> getUsers(String role) throws UserManagementException {
        List<User> userList = new ArrayList<>();
        Connection dbConnection = null;
        PreparedStatement prepStmt = null;

        String sqlStmt = "SELECT "
                + " u.UM_ID AS uid, "
                + " u.UM_USER_NAME AS username, "
                + " GROUP_CONCAT(DISTINCT CONCAT_WS(':',att.UM_ATTR_NAME, att.UM_ATTR_VALUE)) AS attributes, "
                + " GROUP_CONCAT(DISTINCT r.UM_ROLE_NAME) AS roles "
                + "FROM "
                + " (UM_USER u LEFT JOIN UM_USER_ATTRIBUTE att ON u.UM_ID = att.UM_USER_ID), "
                + " UM_USER_ROLE ur, "
                + " UM_ROLE r "
                + "WHERE "
                + " u.UM_ID = ur.UM_USER_ID "
                + " AND ur.UM_ROLE_ID = r.UM_ID "
                + " AND r.UM_ROLE_NAME = ? "
                + "GROUP BY uid;";

        try {
            dbConnection = DataBaseUtil.getDBConnection();
            prepStmt = dbConnection.prepareStatement(sqlStmt);
            prepStmt.setString(1, role);
            ResultSet rs = prepStmt.executeQuery();

            while (rs.next()) {
                User user = new User();
                user.setUserName(rs.getString(COLUMN_LABEL_USERNAME));
                String attributes = rs.getString(COLUMN_LABEL_ATTRIBUTES);

                if (StringUtils.isNotEmpty(attributes)) {
                    Map<String, String> claims = Pattern.compile(",").splitAsStream(attributes)
                            .map(attribute -> attribute.split(":")).collect(Collectors
                                    .toMap(attribute -> attribute[0], attribute -> attribute[1], (p1, p2) -> p1));
                    user.setClaims(claims);
                }

                String roles = rs.getString(COLUMN_LABEL_ROLES);
                if (roles != null) {
                    String[] roleList = Pattern.compile(",").splitAsStream(roles).toArray(String[]::new);
                    user.setRoles(roleList);
                }
                userList.add(user);
            }
        } catch (SQLException e) {
            throw new UserManagementException("Error occurred while retrieving the user list.", e);
        } finally {
            DataBaseUtil.closeAllConnections(dbConnection, prepStmt);
        }
        return userList;
    }

    /**
     * Returns all users as a List
     *
     * @return List of {@link User}
     * @throws UserManagementException if unable to retrieve user list.
     */
    @Override
    public List<User> getAllUsers() throws UserManagementException {
        List<User> userList = new ArrayList<>();
        Connection dbConnection = null;
        PreparedStatement prepStmt = null;

        String sqlStmt = "SELECT "
                + " u.UM_ID AS uid, "
                + " u.UM_USER_NAME AS username, "
                + " GROUP_CONCAT(DISTINCT CONCAT_WS(':',att.UM_ATTR_NAME, att.UM_ATTR_VALUE)) AS attributes, "
                + " GROUP_CONCAT(DISTINCT r.UM_ROLE_NAME) AS roles "
                + "FROM "
                + " (UM_USER u LEFT JOIN UM_USER_ATTRIBUTE att ON u.UM_ID = att.UM_USER_ID), "
                + " UM_USER_ROLE ur, "
                + " UM_ROLE r "
                + "WHERE "
                + " u.UM_ID = ur.UM_USER_ID "
                + " AND ur.UM_ROLE_ID = r.UM_ID "
                + " AND r.UM_ROLE_NAME != 'system' "
                + "GROUP BY uid;";

        try {
            dbConnection = DataBaseUtil.getDBConnection();
            prepStmt = dbConnection.prepareStatement(sqlStmt);
            ResultSet rs = prepStmt.executeQuery();

            while (rs.next()) {
                User user = new User();
                user.setUserName(rs.getString(COLUMN_LABEL_USERNAME));
                String attributes = rs.getString(COLUMN_LABEL_ATTRIBUTES);

                if (StringUtils.isNotEmpty(attributes)) {
                    Map<String, String> claims = Pattern.compile(",").splitAsStream(attributes)
                            .map(attribute -> attribute.split(":")).collect(Collectors
                                    .toMap(attribute -> attribute[0], attribute -> attribute[1], (p1, p2) -> p1));
                    user.setClaims(claims);
                }

                String roles = rs.getString(COLUMN_LABEL_ROLES);
                if (roles != null) {
                    String[] roleList = Pattern.compile(",").splitAsStream(roles).toArray(String[]::new);
                    user.setRoles(roleList);
                }
                userList.add(user);
            }
        } catch (SQLException e) {
            throw new UserManagementException("Error occurred while retrieving the user list.", e);
        } finally {
            DataBaseUtil.closeAllConnections(dbConnection, prepStmt);
        }

        return userList;
    }

    @Override
    public void updateUser(User user) throws UserManagementException {
        User existingUser = getUser(user.getUserName());
        Connection dbConnection;
        try {
            dbConnection = DataBaseUtil.getDBConnection();
        } catch (SQLException e) {
            String errorMessage = "Error occurred while getting DB connection";
            log.error(errorMessage, e);
            throw new UserManagementException(errorMessage, e);
        }

        Secret credentialObj = null;

        try {
            String password = user.getPassword();
            if (password != null) {

                credentialObj = Secret.getSecret(user.getPassword());
                String sqlStmt = "UPDATE UM_USER SET UM_USER_PASSWORD = ?, UM_SALT_VALUE = ?, "
                                 + "UM_CHANGED_TIME = ? WHERE UM_USER_NAME = ?";

                String saltValue = CommonUtil.generateSaltValue();
                password = CommonUtil.preparePassword(credentialObj, saltValue);

                // do all 4 possibilities
                if (saltValue == null) {
                    CommonUtil.updateStringValuesToDatabase(dbConnection, sqlStmt, password, "", new Date(),
                                                            user.getUserName());
                } else {
                    CommonUtil.updateStringValuesToDatabase(dbConnection, sqlStmt, password, saltValue,
                                                            new Date(), user.getUserName());
                }
            }

            if (user.getRoles() != null && user.getRoles().length > 0) {

                String[] roles = user.getRoles();
                String[] existingRoles = existingUser.getRoles();

                if (!Arrays.deepEquals(roles, existingRoles)) {

                    String sqlStmt;
                    //remove old roles
                    if (existingRoles.length > 0) {
                        sqlStmt = "DELETE FROM UM_USER_ROLE WHERE UM_ROLE_ID = (SELECT UM_ID FROM UM_ROLE WHERE " +
                                  "UM_ROLE_NAME = ?) AND UM_USER_ID = (SELECT UM_ID FROM UM_USER WHERE " +
                                  "LOWER(UM_USER_NAME)=LOWER(?))";
                        DataBaseUtil.removeUserRoleMapping(dbConnection, sqlStmt, existingRoles, user.getUserName());
                    }

                    //set new roles
                    if (roles.length > 0) {
                        // Adding user to the non shared roles
                        sqlStmt = "INSERT INTO UM_USER_ROLE (UM_ROLE_ID, "
                                  + "UM_USER_ID) VALUES ((SELECT UM_ID FROM UM_ROLE WHERE UM_ROLE_NAME=?),"
                                  + "(SELECT UM_ID FROM UM_USER WHERE LOWER(UM_USER_NAME)=LOWER(?)))";
                        DataBaseUtil.updateUserRoleMappingInBatchMode(dbConnection, sqlStmt, roles, user.getUserName());
                    }
                }
            }

            if (user.getClaims() != null) {
                Map<String, String> existingClaims = existingUser.getClaims();
                Map<String, String> newClaims = user.getClaims();
                Map<String, String> claimsToUpdate = new HashMap<>();
                Map<String, String> claimsToInsert =new HashMap<>();
                for (String key : newClaims.keySet()) {
                    if (existingClaims.containsKey(key)) {
                        claimsToUpdate.put(key, newClaims.get(key));
                    } else {
                        claimsToInsert.put(key, newClaims.get(key));
                    }
                }
                if (!claimsToUpdate.isEmpty()) {
                    DataBaseUtil.updateUserAttributes(dbConnection, user.getUserName(), claimsToUpdate);
                }
                if (!claimsToInsert.isEmpty()) {
                    DataBaseUtil.addUserAttributes(dbConnection, user.getUserName(), claimsToInsert);
                }
            }

            dbConnection.commit();
        } catch (Exception e) {
            try {
                dbConnection.rollback();
            } catch (SQLException e1) {
                String errorMessage = "Error roll backing update user operation for user : " + user.getUserName();
                log.debug(errorMessage, e1);
            }
            String errorMessage = "Error while updating user : " + user.getUserName();
            log.debug(errorMessage, e);
            throw new UserManagementException(errorMessage, e);
        } finally {
            if (credentialObj != null) {
                credentialObj.clear();
            }
            DataBaseUtil.closeAllConnections(dbConnection);
        }
    }

    @Override
    public void deleteUser(String username) throws UserManagementException {
        Connection dbConnection = null;
        PreparedStatement prepStmt = null;
        int userId = 0;

        String sqlStmt = "SELECT UM_ID AS uid FROM UM_USER WHERE UM_USER_NAME=?";

        try {
            dbConnection = DataBaseUtil.getDBConnection();
            prepStmt = dbConnection.prepareStatement(sqlStmt);
            prepStmt.setString(1, username);

            ResultSet rs = prepStmt.executeQuery();
            if (rs.next()) {
                userId = rs.getInt("uid");
            }

            if (userId > 0) {
                sqlStmt = "DELETE FROM UM_USER_ATTRIBUTE WHERE UM_USER_ID=?";
                prepStmt = dbConnection.prepareStatement(sqlStmt);
                prepStmt.setInt(1, userId);
                prepStmt.executeUpdate();

                sqlStmt = "DELETE FROM UM_USER_ROLE WHERE UM_USER_ID=?";
                prepStmt = dbConnection.prepareStatement(sqlStmt);
                prepStmt.setInt(1, userId);
                prepStmt.executeUpdate();

                sqlStmt = "DELETE FROM UM_USER WHERE UM_ID=?";
                prepStmt = dbConnection.prepareStatement(sqlStmt);
                prepStmt.setInt(1, userId);
                prepStmt.executeUpdate();
            }
            dbConnection.commit();
        } catch (SQLException e) {
            try {
                if (dbConnection != null) {
                    dbConnection.rollback();
                }
            } catch (SQLException e1) {
                String errorMessage = "Error roll backing delete user operation for : " + username;
                if (log.isDebugEnabled()) {
                    log.debug(errorMessage, e1);
                }
            }
            throw new UserManagementException("Error occurred while deleting the user: " + username);
        } finally {
            DataBaseUtil.closeAllConnections(dbConnection, prepStmt);
        }
    }

    @Override
    public boolean isUserExist(String username) throws UserManagementException {
        Connection dbConnection = null;
        PreparedStatement prepStmt = null;
        boolean userExists = false;

        String sqlStmt = "SELECT EXISTS (SELECT UM_ID FROM UM_USER WHERE UM_USER_NAME=?) AS user_status";

        try {
            dbConnection = DataBaseUtil.getDBConnection();
            prepStmt = dbConnection.prepareStatement(sqlStmt);
            prepStmt.setString(1, username);

            ResultSet rs = prepStmt.executeQuery();
            if(rs.next()) {
                userExists = rs.getBoolean("user_status");
            }
        } catch (SQLException e) {
            throw new UserManagementException("Error occurred while checking the existence of user: " + username);
        } finally {
            DataBaseUtil.closeAllConnections(dbConnection, prepStmt);
        }

        return userExists;
    }

    @Override
    public void addRole(String roleName, boolean sharedRole) throws UserManagementException, SQLException {
        Connection dbConnection = null;
        PreparedStatement prepStmt = null;

        String sqlStmt = "INSERT INTO UM_ROLE (UM_ROLE_NAME, UM_SHARED_ROLE) VALUES (?, ?);";

        try {
            dbConnection = DataBaseUtil.getDBConnection();
            prepStmt = dbConnection.prepareStatement(sqlStmt);
            prepStmt.setString(1, roleName);
            prepStmt.setBoolean(2, sharedRole);

            prepStmt.executeUpdate();
            dbConnection.commit();
        } catch (SQLException e) {
            try {
                if (dbConnection != null) {
                    dbConnection.rollback();
                }
            } catch (SQLException e1) {
                String errorMessage = "Error roll backing add role operation for : " + roleName;
                if (log.isDebugEnabled()) {
                    log.debug(errorMessage, e1);
                }
            }
            throw new UserManagementException("Error while persisting role : " + roleName, e);
        } finally {
            DataBaseUtil.closeAllConnections(dbConnection, prepStmt);
        }
    }
}
