/*
 *  Copyright (c) 2019 Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 *  Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 *  Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 */

package io.entgra.iot.fog.usermgt.authenticator;

import io.entgra.iot.fog.usermgt.beans.AuthenticationType;
import io.entgra.iot.fog.usermgt.beans.UserCredential;
import io.entgra.iot.fog.usermgt.exceptions.AuthenticationException;
import io.entgra.iot.fog.usermgt.utils.CommonUtil;
import io.entgra.iot.fog.usermgt.utils.Constants;
import io.entgra.iot.fog.usermgt.utils.DataBaseUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.Date;
import java.util.GregorianCalendar;

public class BasicAuthAuthenticator {
    private static final Logger log = LogManager.getLogger(BasicAuthAuthenticator.class);

    public boolean doAuthenticate(UserCredential credential, AuthenticationType type) throws AuthenticationException {

        Connection dbConnection = null;
        ResultSet rs = null;
        PreparedStatement prepStmt = null;
        String sqlStmt;
        String password;
        boolean isAuth = false;

        try {
            dbConnection = DataBaseUtil.getDBConnection();
            dbConnection.setAutoCommit(false);

            if (Constants.AUTH_TYPE_MOBILE.equals(type.toString())) {
                sqlStmt = "SELECT * FROM UM_USER WHERE UM_ID = (SELECT UM_USER_ID FROM UM_USER_ATTRIBUTE " +
                        "WHERE UM_ATTR_VALUE=?)";
            } else {
                sqlStmt = "SELECT * FROM UM_USER WHERE LOWER(UM_USER_NAME)=LOWER(?)";
            }

            if (log.isDebugEnabled()) {
                log.debug(sqlStmt);
            }

            prepStmt = dbConnection.prepareStatement(sqlStmt);
            prepStmt.setString(1, credential.getUsername());

            rs = prepStmt.executeQuery();

            if (rs.next()) {
                String storedPassword = rs.getString(3);
                String saltValue = rs.getString(4);

                boolean requireChange = rs.getBoolean(5);
                Timestamp changedTime = rs.getTimestamp(6);

                GregorianCalendar gc = new GregorianCalendar();
                gc.add(GregorianCalendar.HOUR, -24);
                Date date = gc.getTime();

                if (requireChange && changedTime.before(date)) {
                    isAuth = false;
                } else {
                    password = CommonUtil.preparePassword(credential.getPassword(), saltValue);
                    if ((storedPassword != null) && (storedPassword.equals(password))) {
                        isAuth = true;
                    }
                }
            }
        } catch (SQLException e) {
            String msg =
                    "Error occurred while retrieving user authentication info for user : " + credential.getUsername();
            throw new AuthenticationException(msg, e);
        } catch (Exception e) {
            String msg = "Error occurred while authenticating user : " + credential.getUsername();
            if (log.isDebugEnabled()) {
                log.debug(msg, e);
            }
            throw new AuthenticationException(msg, e);
        } finally {
            DataBaseUtil.closeAllConnections(dbConnection, rs, prepStmt);
        }
        return isAuth;
    }
}
