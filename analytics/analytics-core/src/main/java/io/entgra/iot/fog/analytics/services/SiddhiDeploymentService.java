/*
 *  Copyright (c) 2019 Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 *  Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 *  Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 */

package io.entgra.iot.fog.analytics.services;

import io.entgra.iot.fog.analytics.utils.AnalyticsDataHolder;
import io.entgra.iot.fog.analytics.utils.Constants;
import io.entgra.iot.fog.analytics.utils.Utils;
import io.entgra.iot.fog.analytics.beans.SiddhiQuery;
import org.wso2.msf4j.formparam.FormDataParam;

import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Path("/siddhi-deployer")
public class SiddhiDeploymentService {

    /**
     * Allows for deploying a new siddhi query for analytics
     *
     * @param qName Unique query name
     * @param query Query being deployed
     * @return HTTP response with success or failure of query deployment
     */

    @POST
    @Path("/query")
    public Response deploy(@FormDataParam("qName") String qName, @FormDataParam("query") String query) {
        try {
            java.nio.file.Path path = Paths.get(Constants.SIDDHI_QUERY_DIRECTORY + qName);
            if (Files.notExists(path)) {
                Files.write(Paths.get(Constants.SIDDHI_QUERY_DIRECTORY + qName), query.getBytes());
                SiddhiQuery siddhiQuery = new SiddhiQuery();
                siddhiQuery.setName(qName);
                siddhiQuery.setContent(query);
                AnalyticsDataHolder.getInstance().getSiddhiEngine().deployQuery(siddhiQuery);
                return Response.ok().build();
            } else {
                return Response.status(Response.Status.BAD_REQUEST).entity("Query '" + qName + "' already exists.").
                        build();
            }
        } catch (IOException e) {
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    /**
     * Retrieves all siddhi queries related to the context
     *
     * @return JSON list with all queries
     */

    @GET
    @Path("/queries")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllQueries() {
        try {
            return Response.ok().entity(Utils.getAllQueries()).build();
        } catch (IOException e) {
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    /**
     * Retrieves siddhi query details based on assigned query name
     *
     * @param qName Assigned query name during deployment
     * @return SiddhiQuery object with query details
     */

    @GET
    @Path("/query/{qName}")
    public Response getQuery(@PathParam("qName") String qName) {
        try {
            SiddhiQuery siddhiQuery = Utils.getQuery(qName);
            if (siddhiQuery != null) {
                return Response.ok().entity(siddhiQuery).build();
            } else {
                return Response.status(Response.Status.BAD_REQUEST).entity("Query '" + qName + "' not exists.").build();
            }
        } catch (IOException e) {
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    /**
     * Update Siddhi query with new query body
     *
     * @param qName Retrieve siddhi query using unique query name
     * @param query Update query body
     * @return HTTP response with success or failure of query update
     */

    @PUT
    @Path("/query/{qName}")
    public Response updateQuery(@PathParam("qName") String qName, @FormParam("query") String query) {
        try {
            java.nio.file.Path path = Paths.get(Constants.SIDDHI_QUERY_DIRECTORY + qName);
            if (Files.exists(path)) {
                Files.write(Paths.get(Constants.SIDDHI_QUERY_DIRECTORY + qName), query.getBytes());
                SiddhiQuery siddhiQuery = new SiddhiQuery();
                siddhiQuery.setName(qName);
                siddhiQuery.setContent(query);
                AnalyticsDataHolder.getInstance().getSiddhiEngine().redeployQuery(siddhiQuery);
                return Response.ok().build();
            } else {
                return Response.status(Response.Status.BAD_REQUEST).entity("Query '" + qName + "' not exists.").build();
            }
        } catch (IOException e) {
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    /**
     * Delete a deployed query
     *
     * @param qName Unique query name assigned during deployment
     * @return HTTP response with success or failure of query deletion
     */

    @DELETE
    @Path("/query/{qName}")
    public Response deleteQuery(@PathParam("qName") String qName) {
        try {
            if (Files.deleteIfExists(Paths.get(Constants.SIDDHI_QUERY_DIRECTORY + qName))) {
                AnalyticsDataHolder.getInstance().getSiddhiEngine().unDeployQuery(qName);
                return Response.ok().build();
            } else {
                return Response.status(Response.Status.BAD_REQUEST).entity("Query '" + qName + "' not exists.").build();
            }
        } catch (IOException e) {
            return Response.serverError().entity(e.getMessage()).build();
        }
    }
}
