/*
 *  Copyright (c) 2019 Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 *  Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 *  Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 */

package io.entgra.iot.fog.analytics;

import org.h2.tools.Server;

import java.sql.DriverManager;
import java.sql.SQLException;

public class DBEngine {

    public static void startDBServer() throws SQLException, ClassNotFoundException {
        Class.forName("org.h2.Driver");
        Server.createTcpServer("-tcpPort", "9092", "-tcpAllowOthers").start();
        DriverManager.getConnection("jdbc:h2:tcp://localhost:9092/~/iot-fog/event-store", "admin", "admin");
    }

    public static void stopDBServer() throws SQLException {
        Server.shutdownTcpServer("tcp://localhost:9092", "", true, true);
    }

}
