#!/bin/bash
# Copyright (C) 2019, Entgra (Pvt) Ltd - All Rights Reserved.
# Unauthorised copying/redistribution of this file, via any medium is strictly prohibited
# Proprietary and confidential.

# Licensed under the Entgra Commercial License, Version 1.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# https://entgra.io/licenses/entgra-commercial/1.0

java -jar analytics.jar
